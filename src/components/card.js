import React, {Component} from 'react'
//import ToggleButton from './toggle_button'
import './card.scss'

class Card extends Component{
    constructor(props){
        super(props)
        this.state = {
        }
    }
    // handleLikes = (likeStatus,id) => {
    //     console.log(likeStatus,id)
    // }

    render(){
        //const content = this.props.content ? this.props.content : ""
        const { content, onDelete, updateLikes } = this.props
        const { 
            title = "", 
            category = "", 
            likes = "", 
            dislikes = "",
            id = "" ,
        } = content
        console.log("updatelikes",)
        return(
            <div className = "custom-card">
                <p>{category}</p>
                <div className = "inline">
                    <p className = 'likes'>{likes}</p>
                    <p className = "likes dislikes">{dislikes}</p>
                </div>
                <button onClick = {onDelete} value = {id}>Delete</button>
                <ToggleButton 
                    id = {id}
                    upDateLikes = {updateLikes}
                />
                <h4>{title}</h4>
            </div>
        )
    }
}
export default Card

class ToggleButton extends Component{
    constructor(props){
        super(props)
        this.state = {
            like:false
        }
    }
    handleClick = () => {
        const { upDateLikes } = this.props

        this.setState((prevState) => {
            return {
                like:!prevState.like
            }
        },() => upDateLikes(this.state.like,this.props.id))
        
    }

    render(){
        var content = this.state.like ? "dislike" : "like"
        return(
            <button onClick= {this.handleClick} value = {this.props.id}>{content}</button>
        ) 
    }   
    
}