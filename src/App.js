import React, { Component } from 'react';
import movies$ from './data/movies'
import './App.css';
import Card from './components/card'

class App extends Component {
  constructor(props){
    super(props)
    this.state = {}
    movies$.then((value)=>this.setState({movies:value}))
  }

  handleDelete = (id) => {
    const movieId = parseInt(id.target.value)
    //console.log(parseInt(id.target.value))
    const newState = this.state.movies.filter(movie => parseInt(movie.id) !== movieId)
    this.setState({movies:newState},()=>console.log(this.state.movies))
  }

  handleLikes = (likeStatus,id) => {
    const newState = this.state.movies.map((movie,index) => {
      if((movie.id === id) && likeStatus === true) {
        return {
          ...movie,
          likes: movie.likes + 1,
        }
      }
      else if((movie.id === id) && likeStatus === false){
        return {
          ...movie,
          likes: movie.likes - 1,
        }
      }
      else{
        return movie
      }
    })
    this.setState({movies:newState})
  }


  render() {
    const { movies } = this.state
    var categories = movies ? movies.reduce((acc,movie)=>{
      console.log(movie.category,acc)
      //if(!acc.includes(movie.category)){
        return acc+movie.category
      //}
    },[]) : []
    console.log("categories",categories)
    return (
     <div className="card-container">
      {/* {this.state.movies && <Filter />} */}
       {this.state.movies && this.state.movies.map((movie,index) => 
         <Card 
            key = {index+movie}
            content = {this.state.movies[index]}
            onDelete = {this.handleDelete}
            updateLikes = {this.handleLikes}
         />
       )}
     </div>
    );
  }
}

export default App;
